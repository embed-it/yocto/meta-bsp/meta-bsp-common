SUMMARY = "Embed-it development image"

require embedit-image-base.bb

inherit extrausers

EXTRA_USERS_PARAMS = "usermod -P root root;"

IMAGE_FEATURES += "ssh-server-openssh tools-debug debug-tweaks"

CORE_IMAGE_EXTRA_INSTALL += "ethtool evtest fbset i2c-tools memtester"

IMAGE_INSTALL_append = " \
        openssh-sftp-server \
	set-static-ip \	
        "
