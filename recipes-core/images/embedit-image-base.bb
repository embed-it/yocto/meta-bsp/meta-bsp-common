SUMMARY = "Embed-it production image"

LICENSE = "GPLv2"

include recipes-core/images/core-image-minimal.bb

IMAGE_FEATURES += "splash"

CORE_IMAGE_EXTRA_INSTALL = "inotify-tools"

IMAGE_INSTALL_append = " \
        packagegroup-core-boot \
	"
